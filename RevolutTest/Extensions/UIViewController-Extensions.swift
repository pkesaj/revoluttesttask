//
//  UIViewController-Extensions.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static func pickTopViewController() -> UIViewController {
        let base = UIApplication.shared.keyWindow?.rootViewController
        
        if let navController = base as? UINavigationController,
            let visibleVC = navController.visibleViewController {
            return visibleVC
        }
        
        if let tabController = base as? UITabBarController,
            let selected = tabController.selectedViewController {
            return selected.presentedViewController ?? selected
        }
        
        print("WARNING: VIEWCONTROLLER LAUNCHED FROM ROOTVIEWCONTROLLER")
        return base?.presentedViewController ?? UIViewController()
    }
}
