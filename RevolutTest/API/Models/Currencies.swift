//
//  Currencies.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import Foundation

struct Currencies: Decodable {
    var base: String
    var date: String
    var rates: [String: Double]
}
