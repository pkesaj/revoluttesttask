//
//  Errors.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import Foundation

extension API {
    
    enum APIError: Swift.Error {
        case invalidUrl
    }
}
