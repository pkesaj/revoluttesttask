//
//  ServerEnvironment.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import Foundation

extension API {
    
    enum ServerEnvironment {
        case test
        
        func getServerAdress() -> String {
            switch self {
            case .test:
                return "https://revolut.duckdns.org"
            }
        }
        
        func getLogin() -> String {
            switch self {
            case .test:
                return ""
            }
        }
        
        func getPassword() -> String {
            switch self {
            case .test:
                return ""
            }
        }
    }
}
