//
//  Response.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import Foundation

extension API {
    
    struct Response<T: Decodable> {
        var result: T?
        var error: Error?
        var statusCode: Int?
        
        init(result: T? = nil, error: Error? = nil, statusCode: Int? = nil) {
            self.result = result
            self.error = error
            self.statusCode = statusCode
        }
    }
}
