//
//  API.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

//start at 22:14
struct API {
    
    private static let apiDispatch = DispatchQueue(label: "pk.RevolutTest.apiDispatch", qos: DispatchQoS.utility, attributes: DispatchQueue.Attributes.concurrent, autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.workItem, target: nil)
    
    
    //usually i will use alamofire(geturlcomponent protocol or sth like that) but it's only one request with get param so..
    
    public static func load<T: Decodable>(_ request: API.Router<T>, completion: @escaping (Response<T>) -> Void) {
        apiDispatch.async {
            
            Helpers.turnOnNetworkActivity()
            
            guard let url = URL(string: request.getUrlString()) else {
                Helpers.turnOffNetworkActivity()
                completion(Response<T>(error: APIError.invalidUrl))
                return
            }
            
            //Here I will use Reachability pod to check the internet connection but i am in the train right now and i don't have internet, yes in Poland trains we don't have the internet(stable connection)
            let urlRequest = URLRequest(url: url,
                                        cachePolicy: .useProtocolCachePolicy,
                                        timeoutInterval: Statics.requestTimeout)
            
            let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                
                Helpers.turnOffNetworkActivity()
                let statusCode = (response as? HTTPURLResponse)?.statusCode
                guard error == nil, let data = data else {
                    self.handleError(statusCode: statusCode, error: error, shouldTryAgain: { tryAgain in
                        API.load(request, completion: { response in
                            completion(response)
                        })
                    })
                    return
                }
                
                do {
                    let decodedData = try JSONDecoder().decode(T.self, from: data)
                    completion(Response(result: decodedData, error: error, statusCode: statusCode))
                } catch {
                    completion(Response(error: error, statusCode: statusCode))
                }
            })
            task.resume()
        }
    }
    
    private static func handleError(statusCode: Int?, error: Error?, shouldTryAgain: @escaping (Bool) -> (Void)) {
        guard statusCode == 200 else {
            AlertFactory.showAlert(title: "error".localized(), message: "unexpectedErrorMessage".localized()) { tryAgain in
                shouldTryAgain(tryAgain)
            }
            return
        }
        //Error handling switch or sth else, usually to localized i'm using R.Swift
    }
}
