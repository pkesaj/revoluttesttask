//
//  Router.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import Foundation

extension API {
    
    enum Router<T: Decodable> {
        case currenciesRates(base: String?)
        
        func getUrlString() -> String {
            let serverAdress = Statics.serverEnvironment.getServerAdress()
            switch self {
            case .currenciesRates(base: let currency):
                if let baseCurrency = currency {
                    return serverAdress + "/latest?base=\(baseCurrency)"
                } else {
                    return serverAdress + "/latest"
                }
            }
        }
    }
}
