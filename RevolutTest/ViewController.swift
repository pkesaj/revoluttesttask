//
//  ViewController.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    weak var navController: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.addButton()
    }
    
    private func addButton() {
        let button = UIButton()
        self.view.addSubview(button)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true

        button.addTarget(self, action: #selector(showCurrenciesVC), for: .touchUpInside)
        button.setTitle("openCurrenciesViewController".localized(), for: .normal)
        button.setTitleColor(.black, for: .normal)
    }
    
    @objc private func showCurrenciesVC() {
        let currenciesVC = CurrenciesViewControler()
        let navigationController = UINavigationController(rootViewController: currenciesVC)
        
        navigationController.modalPresentationStyle = .currentContext
        self.present(navigationController, animated: true, completion: nil)
        self.navController = navigationController
    }
}

