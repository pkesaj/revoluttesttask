//
//  AlertFactory.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

struct AlertFactory {
    
    static func showAlert(title: String, message: String, shouldTryAgain: ((Bool) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let action = UIAlertAction(title: "ok".localized(), style: .default) { _ in
                shouldTryAgain?(false)
            }
            alert.addAction(action)
            
            if shouldTryAgain != nil {
                let action2 = UIAlertAction(title: "tryAgain".localized(), style: UIAlertAction.Style.default) { _ in
                    shouldTryAgain?(true)
                }
                alert.addAction(action2)
            }
            UIViewController.pickTopViewController().present(alert, animated: true, completion: nil)
        }
    }
}
