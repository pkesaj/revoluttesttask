//
//  Helpers.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

struct Helpers {
    
    static let turnOnNetworkActivity: () -> Void = {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    static let turnOffNetworkActivity: () -> Void = {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}
