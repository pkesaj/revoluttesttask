//
//  Statics.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import Foundation

struct Statics {
    
    static let serverEnvironment = API.ServerEnvironment.test
    static let requestTimeout: TimeInterval = 10
}

extension Statics {
    
    struct Currencies {
        static let refreshTime: TimeInterval = 1
    }
}
