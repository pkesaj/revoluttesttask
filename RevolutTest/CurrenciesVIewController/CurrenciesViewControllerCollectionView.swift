//
//  CurrenciesViewControllerCollectionView.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

extension CurrenciesViewControler: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CurrencyCell.ElementSize.getCellSize(collectionView)
    }
}

extension CurrenciesViewControler: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.collectionView?.visibleCells.forEach{
            ($0 as? CurrencyCell)?.resignFirstResponder()
        }
    }
}

extension CurrenciesViewControler: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let itemsCount = self.currenciesModule?.calculatedCurrencies.count else { return 0 }
        return itemsCount + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let model = self.getModel(indexPath),
            let cell = cell as? CurrencyCell {
            cell.key = model.key
            cell.value = model.value
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CurrencyCell.identifier, for: indexPath) as? CurrencyCell
        cell?.delegate = self
        if cell?.key == nil, let model = self.getModel(indexPath) {
            cell?.key = model.key
            cell?.value = model.value
        }
        
        if cell?.key == self.currenciesModule?.actualCurrency {
            cell?.isActive = true
        } else {
            cell?.isActive = false
        }
        
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? CurrencyCell {
            cell.key = nil
            cell.value = nil
        }
    }
    
    private func getModel(_ indexPath: IndexPath) -> (key: String, value: Double)? {
        let convertedIndexPath = indexPath.row - 1
        guard let model = self.currenciesModule?.calculatedCurrencies,
            model.count > convertedIndexPath else { return nil }
        
        if indexPath.row == 0 {
            return (key: self.currenciesModule?.actualCurrency ?? "", value: self.baseValue)
        }
        
        return model[convertedIndexPath]
    }
}

extension CurrenciesViewControler {
    
    func moveToTop(_ cell: CurrencyCell) {
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        self.collectionView?.moveItem(at: indexPath, to: IndexPath(row: 0, section: 0))
        
        if let visibleCell = self.collectionView?.visibleCells,
            let index = visibleCell.index(where: { ($0 as? CurrencyCell)?.isActive == true }) {
            (visibleCell[index] as? CurrencyCell)?.isActive = false
        }
        cell.isActive = true
    }
}
