//
//  CurrenciewViewController.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

class CurrenciesViewControler: UIViewController {
    
    private var refreshTimer: Timer?

    var baseValue: Double = 100
    
    var currenciesModule: CurrenciesModule? {
        didSet {
            DispatchQueue.main.async {
                self.activityIndicator?.removeFromSuperview()
            }
        }
    }
    
    weak var collectionView: UICollectionView?
    weak var activityIndicator: UIActivityIndicatorView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startRefreshTimer()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "currenciesViewControllerTitle".localized()
        self.setupLayout()
        self.setupCollectionView()
        self.addLoadingIndicator()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.refreshTimer?.invalidate()
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        self.view.addSubview(collectionView)
        self.collectionView = collectionView
        
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        self.registerCells()
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    private func startRefreshTimer() {
        self.requestForCurrencies()
        self.refreshTimer = Timer.scheduledTimer(withTimeInterval: Statics.Currencies.refreshTime, repeats: true, block: { [weak self] _ in
            self?.requestForCurrencies()
        })
    }
    
    private func requestForCurrencies(baseKey: String? = nil) {
        API.load(API.Router<Currencies>.currenciesRates(base: baseKey ?? self.currenciesModule?.actualCurrency)) { [weak self] response in
            
            guard response.error == nil, response.statusCode == 200, let result = response.result else {
                AlertFactory.showAlert(title: "error".localized(), message: "unexpected".localized())
                return
            }
            
            if let currenciesModule = self?.currenciesModule {
                currenciesModule.currencies = result
            } else {
                self?.createCurrenciesModule(result)
            }
        }
    }
    
    private func createCurrenciesModule(_ currencies: Currencies) {
        let module = CurrenciesModule(currencies: currencies, delegate: self)
        
        self.currenciesModule = module
    }
    
    private func registerCells() {
        //in NIB case I will use R.Swift
        self.collectionView?.register(CurrencyCell.self, forCellWithReuseIdentifier: CurrencyCell.identifier)
    }
    
    private func setupLayout() {
        self.view.backgroundColor = .white
    }
    
    private func addLoadingIndicator() {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        self.view.addSubview(activityIndicator)
        self.activityIndicator = activityIndicator
        
        activityIndicator.startAnimating()
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
}

extension CurrenciesViewControler: CurrencyCellDelegate {
    
    func activate(cell: CurrencyCell) {
        self.requestForCurrencies(baseKey: cell.key)
        self.moveToTop(cell)
    }
    
    func updateBaseValue(key: String, value: Double) {
        self.baseValue = value
        self.updateCurrencies()
    }
}

extension CurrenciesViewControler: CurrenciesModuleDelegate {
    
    func updateCurrencies() {
        DispatchQueue.main.async {
            let visibleCells = self.collectionView?.visibleCells
            
            guard visibleCells?.count ?? 0 > 0 else {
                self.collectionView?.reloadData()
                return
            }
            
            visibleCells?.forEach({ cell in
                guard let currencyCell = cell as? CurrencyCell,
                    !currencyCell.isActive,
                    let cellKey = currencyCell.key else { return }
                
                currencyCell.value = self.currenciesModule?.getValueOf(key: cellKey, baseValue: self.baseValue)
            })
        }
    }
}
