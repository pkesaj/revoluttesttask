//
//  CurrencyCell.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 22/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import UIKit

protocol CurrencyCellDelegate: class {
    func activate(cell: CurrencyCell)
    func updateBaseValue(key: String, value: Double)
}

class CurrencyCell: UICollectionViewCell {

    static let identifier: String = "currencyCell"
    
    private let activeBottomLineColor = UIColor.green
    private let inActiveBottomLineColor = UIColor.gray
    
    private let activeImageViewColor = UIColor.green
    private let inActiveImageViewColor = UIColor.red
    
    var key: String? {
        didSet {
            self.label?.text = key
        }
    }
    
    var value: Double? {
        didSet {
            guard let value = self.value else { return }
            let roundedValue = (100 * value).rounded() / 100
            self.textField?.text = String(roundedValue)
        }
    }
    
    weak var delegate: CurrencyCellDelegate?
    
    private weak var imageView: UIImageView?
    private weak var textField: UITextField?
    private weak var label: UILabel?
    private weak var bottomLine: UIView?
    
    var isActive: Bool = false {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addImageView()
        self.addLabel()
        self.addTextField()
        self.setupLayout()
        self.layoutIfNeeded()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.bottomLine?.backgroundColor = (self.isActive) ? self.activeBottomLineColor : self.inActiveBottomLineColor
        self.imageView?.backgroundColor = (self.isActive) ? self.activeImageViewColor : self.inActiveImageViewColor
    }
    
    override var reuseIdentifier: String? {
        return CurrencyCell.identifier
    }
    
    @discardableResult override func resignFirstResponder() -> Bool {
        self.textField?.resignFirstResponder()
        return super.resignFirstResponder()
    }
    
    private func setupLayout() {
        self.backgroundColor = .white
    }
}

extension CurrencyCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.delegate?.activate(cell: self)
    }
    
    @objc private func textFieldValueChanged() {
        let value = Double(self.textField?.text ?? "")
        self.delegate?.updateBaseValue(key: self.key ?? "", value: value ?? 0)
    }
}

//MARK: layout
extension CurrencyCell {
    
    private func addImageView() {
        guard self.imageView == nil else { return }
        let imageView = UIImageView()
        self.addSubview(imageView)
        self.imageView = imageView
        
        imageView.backgroundColor = inActiveImageViewColor
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.8).isActive = true
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor).isActive = true
        
        self.layoutIfNeeded()
        self.maskRoundedImageView(imageView: imageView)
    }
    
    private func addLabel() {
        guard self.label == nil, let imageView = self.imageView else { return }
        
        let label = UILabel()
        self.addSubview(label)
        self.label = label
        
        label.numberOfLines = 1
        label.text = self.key
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 20).isActive = true
        label.widthAnchor.constraint(lessThanOrEqualTo: self.widthAnchor, multiplier: 0.2).isActive = true
    }
    
    private func addTextField() {
        guard self.textField == nil else { return }
        
        let textField = UITextField()
        self.addSubview(textField)
        self.textField = textField
        
        textField.backgroundColor = .clear
        textField.keyboardType = .numberPad
        
        textField.text = String(self.value ?? 0)
        textField.delegate = self
        textField.addTarget(self, action: #selector(self.textFieldValueChanged), for: .editingChanged)
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        textField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        textField.widthAnchor.constraint(lessThanOrEqualTo: self.widthAnchor, multiplier: 0.6).isActive = true
        textField.widthAnchor.constraint(greaterThanOrEqualTo: self.widthAnchor, multiplier: 0.1 ).isActive = true
        
        self.addLineOnTheBottomOfTextField(textField)
    }
    
    private func addLineOnTheBottomOfTextField(_ textField: UITextField) {
        let view = UIView()
        self.addSubview(view)
        self.bottomLine = view
        
        view.backgroundColor = self.inActiveBottomLineColor
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        view.leadingAnchor.constraint(equalTo: textField.leadingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 0).isActive = true
        view.widthAnchor.constraint(equalTo: textField.widthAnchor, multiplier: 1.1).isActive = true
        
    }
    
    private func maskRoundedImageView(imageView: UIImageView) {
        let mask = CAShapeLayer()
        mask.frame = imageView.bounds
        let bezierPath = UIBezierPath(ovalIn: imageView.bounds)
        mask.path = bezierPath.cgPath
        imageView.layer.mask = mask
        imageView.layer.masksToBounds = true
    }
}

extension CurrencyCell {
    
    struct ElementSize {
        
        private static let cellHeight: CGFloat = 80
        
        static func getCellSize(_ collectionView: UICollectionView) -> CGSize {
            return CGSize(width: collectionView.bounds.width - collectionView.contentInset.left - collectionView.contentInset.right, height: cellHeight)
        }
    }
}
