//
//  CurrenciesModule.swift
//  RevolutTest
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import Foundation

protocol CurrenciesModuleDelegate: class {
    var baseValue: Double { get }
    func updateCurrencies()
}

class CurrenciesModule {
    
    var calculatedCurrencies: [(key: String, value: Double)] = []
    
    var currencies: Currencies {
        didSet {
            self.calculatedCurrencies = self.getCalculatedCurrencies(baseValue: self.delegate?.baseValue ?? self.baseValue)
            self.delegate?.updateCurrencies()
        }
    }
    
    var baseValue: Double = 0 {
        didSet {
            self.calculatedCurrencies = self.getCalculatedCurrencies(baseValue: self.baseValue)
        }
    }
    
    var actualCurrency: String {
        return self.currencies.base
    }
    
    private weak var delegate: CurrenciesModuleDelegate?
    
    init(currencies: Currencies, delegate: CurrenciesModuleDelegate?) {
        self.currencies = currencies
        self.calculatedCurrencies = self.getCalculatedCurrencies(baseValue: self.delegate?.baseValue ?? self.baseValue)
        self.delegate = delegate
    }
    
    private func getCalculatedCurrencies(baseValue: Double) -> [(key: String, value: Double)] {
        let otherRates = self.currencies.rates.filter { $0.key != self.actualCurrency }
        
        return otherRates.mapValues({ value -> Double in
            return value * baseValue
        }).map({ (key, value) -> (key: String, value: Double) in
            return (key: key, value: value)
        })
    }
    
    func getValueOf(key: String, baseValue: Double) -> Double? {
        guard let keyRate = self.currencies.rates[key] else { return nil }
        return keyRate * baseValue
    }
}
//I thought It will be bigger :C
