//
//  RevolutTestTests.swift
//  RevolutTestTests
//
//  Created by Piotr Krzesaj on 21/02/2019.
//  Copyright © 2019 Piotr Krzesaj. All rights reserved.
//

import XCTest

class RevolutTestTests: XCTestCase {

    let exampleResponse1: String = """
{"base":"EUR","date":"2018-09-06","rates":{"AUD":1.6224,"BGN":1.9631,"BRL":4.8096,"CAD":1.5395,"CHF":1.1317,"CNY":7.9746,"CZK":25.811,"DKK":7.4844,"GBP":0.90158,"HKD":9.1664,"HRK":7.4617,"HUF":327.7,"IDR":17388.0,"ILS":4.1861,"INR":84.029,"ISK":128.28,"JPY":130.03,"KRW":1309.6,"MXN":22.448,"MYR":4.8299,"NOK":9.8124,"NZD":1.7699,"PHP":62.825,"PLN":4.3344,"RON":4.6558,"RUB":79.871,"SEK":10.63,"SGD":1.606,"THB":38.272,"TRY":7.6566,"USD":1.1677,"ZAR":17.89}}
"""
    let exampleResponse2: String = """
{"base":"EUR","date":"2018-09-06","rates":{"AUD":1.609,"BGN":1.9469,"BRL":4.7699,"CAD":1.5268,"CHF":1.1223,"CNY":7.9088,"CZK":25.598,"DKK":7.4226,"GBP":0.89414,"HKD":9.0907,"HRK":7.4001,"HUF":325.0,"IDR":17244.0,"ILS":4.1515,"INR":83.335,"ISK":127.22,"JPY":128.96,"KRW":1298.8,"MXN":22.263,"MYR":4.79,"NOK":9.7313,"NZD":1.7552,"PHP":62.306,"PLN":4.2986,"RON":4.6173,"RUB":79.211,"SEK":10.542,"SGD":1.5927,"THB":37.956,"TRY":7.5934,"USD":1.1581,"ZAR":17.742}}
"""
    
    var currenciesModules: [CurrenciesModule] = []
    
    override func setUp() {
        
       guard let currencies1 = try? JSONDecoder().decode(Currencies.self, from: exampleResponse1.data(using: .utf8)!),
        let currencies2 = try? JSONDecoder().decode(Currencies.self, from: exampleResponse2.data(using: .utf8)!) else { return }
        
        self.currenciesModules.append(CurrenciesModule(currencies: currencies1, delegate: nil))
        self.currenciesModules.append(CurrenciesModule(currencies: currencies2, delegate: nil))
    }

    override func tearDown() {
        self.currenciesModules = []
    }

    func testDecoding() {
        guard self.currenciesModules.count > 0 else {
            XCTAssert(false, "Error while decoding modules.")
            return
        }
    }
    
    func test0() {
        let baseValue: Double = 0
        self.currenciesModules.forEach { module in
            module.baseValue = baseValue
            XCTAssert(module.getValueOf(key: module.actualCurrency, baseValue: baseValue) == nil, "Base value exist in the module. Currency: \(module.actualCurrency)")
        }
    }
    
    func testNormal() {
        let baseValue: Double = Double.random(in: 0...1000)
        self.currenciesModules.forEach { module in
            module.baseValue = baseValue
            let randomElement = module.currencies.rates.randomElement()
            let calculatedRate = baseValue * (randomElement?.value ?? 0)
            
            XCTAssert(module.getValueOf(key: randomElement?.key ?? "", baseValue: baseValue) == calculatedRate, "Wrong calc baseValue: \(baseValue) key: \(randomElement?.key ?? "") value: \(randomElement?.value ?? 0)")
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
